
@extends('layouts.master')
@section('about_me')
<body>

<!--================ Start Header Area =================-->
<!--================ End Header Area =================-->

<!--================ Start Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-center">
                <h2>{{__('msg.contact')}}</h2>
                <div class="page_link">
                    <a href="{{route('home')}}">{{__('msg.home')}}</a>
                    <a href="{{route('contact.index',app()->getLocale())}}">{{__('msg.contact')}}</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Banner Area =================-->



<!-- ================ contact section start ================= -->
<section class="section-margin">
    <div class="container">



        @if(count($errors)>0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                </ul>
            </div>
            @endif
        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">{{__('msg.click')}}</h2>
            </div>
            <div class="col-lg-8 mb-4 mb-lg-0">
                <form class="form-contact contact_form" action="{{route('contact.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100" name="message"  cols="30" rows="9" placeholder="{{__('msg.message')}}"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="name"  type="text" placeholder="{{__('msg.your_name')}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="email"  type="email" placeholder="{{__('msg.email')}}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <input class="form-control" name="subject"  type="text" placeholder="{{__('msg.subject')}} ">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-lg-3">
                        <button type="submit" class="primary_btn button-contactForm">{{__('msg.send')}}</button>
                    </div>
                </form>


            </div>

            <div class="col-lg-4">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-home"></i></span>
                    <div class="media-body">
                        <h3>{{__('msg.add1')}}</h3>
                        <p>{{__('msg.add2')}}</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3><a href="tel:454545654">09030949922</a></h3>
                        <p>{{__('msg.time_to_answer_phone')}}</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3><a href="mailto:support@colorlib.com">hanavel.ir@gmail.com</a></h3>
                        <p>{{__('msg.time_to_answer_email')}} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->
<!--================Footer Area =================-->
<!--================End Footer Area =================-->
@endsection
