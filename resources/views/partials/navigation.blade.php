
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="{{route('home')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
                <a class="navbar-brand logo_inner_page" href="{{route('home')}}"><img src="{{asset('img/logo2.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav">
                        <li class="nav-item active"><a class="nav-link" href="{{url('/')}}">{{__('home')}}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('about_me',app()->getLocale())}}">{{__('about_me')}} </a></li>
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false">{{__('blog')}}</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="{{route('frontend.posts',app()->getLocale())}}">{{__('blog')}}</a></li>
                            </ul>
                        </li>

                        <li class="nav-item"><a class="nav-link" href="{{route('contact.index',app()->getLocale())}}">{{__('contact')}}</a></li>
                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                               aria-expanded="false">{{__('language')}}</a>
                            <ul class="dropdown-menu">
{{--                                {{route(\Illuminate\Support\Facades\Route::currentRouteName(),'fa')}}--}}
                                <li class="nav-item"><a class="nav-link" href="locale/fa"><img src="{{asset('img/iran.png')}}" alt="" style=" width: 20px;"  > Persian</a></li>
                                <li class="nav-item"><a class="nav-link" href="locale/en"><img src="{{asset('img/usa.png')}}" alt="" style=" width: 20px;"> English </a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>

