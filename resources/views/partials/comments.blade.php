<script type="text/javascript">
    $(".btn-open").on("click",function(){
        $('.form-reply').css('display', 'none');
        var service = this.id;
        var service_id = '#f-' + service;
        $(service_id).show('slow');
    });
</script>
@foreach($comments as $comment)
    <div class="comments-area">

        <div class="comment-list">
            <div class="single-comment justify-content-between d-flex">
                <div class="user justify-content-between d-flex">

                    <div class="desc">
                        <p class="comment">
                            {{$comment->description}}
                        </p>

                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <h5>-
                                    <a href="#">{{$comment->name}}</a>
                                </h5>
                                <p class="date">{{\Hekmatinasser\Verta\Verta::instance($comment->created_at)->formatDatetime()}} </p>
                            </div>

                            <div class="reply-btn">
                                <button class="btn btn-dribbble btn-open" id="div-comment-{{$comment->id}}">پاسخ
                                </button>
                            </div>
                            <div class="form-reply col-md-12" id="f-div-comment-{{$comment->id}}" style="display: none">
                                {!! Form::open(['method' => 'POST', 'route'=> ['frontend.comments.reply']]) !!}
                                @csrf
                                <div class="form-group">
                                    {!! Form::hidden('parent_id', $comment->id) !!}
                                    {!! Form::hidden('post_id', $post->id) !!}

                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="description" id="comment" cols="30" rows="9" placeholder="نظر خود را بنویسید"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="primary_btn button-contactForm">ثبت پاسخ</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            @include('partials.comments', ['comments' => $comment->replies])
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endforeach
