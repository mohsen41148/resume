@extends('layouts.master')
@section('about_me')
    <body>

    <!--================ Start Header Area =================-->

    <!--================ End Header Area =================-->

    <!--================ Start Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-center">
                    <h2>{{__('msg.profile')}}</h2>
                    <div class="page_link">
                        <a href="{{route('home')}}">{{__('msg.home')}}</a>
                        <a href="{{route('about_me')}}">{{__('msg.profile')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Banner Area =================-->

    <!--================Start Portfolio Details Area =================-->
    <section class="portfolio_details_area section-margin">
        <div class="container">
            <div class="row">
                <div class="offset-xl-1 col-xl-10">
                    <div class="portfolio_details_inner">
                        <div class="row">
                            <div class="col-12">
                                <div class="left_img">
                                    <img class="img-fluid" src="img/portfolio/portfolio-details.png" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-7 mb-4 mb-lg-0">
                                <p>{{__('msg.content1')}} </p>
                                <h3  >php& laravel framework ,vue ,sql server ,mysql  </h3>
                                <p>
                                    {{__('msg.content2')}}
                                </p>
                                <p>   {{__('msg.content3')}}</p>
                                <p>{{__('msg.content4')}}  </p>
                            </div>
                            <div class="col-lg-5">
                                <div class="portfolio_right_text mt-30">
                                    <h4 class="text-uppercase">{{__('msg.my_details')}}</h4>

                                    <ul class="list">
                                        <li><span>{{__('msg.name')}}</span>{{__('msg.side1')}}</li>
                                        <li><span>{{__('msg.education')}}</span>{{__('msg.side2')}}</li>
                                        <li><span>{{__('msg.full_stack')}}</span>{{__('msg.side3')}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Portfolio Details Area =================-->

    <!--================ Start Brands Area =================-->
    <section class="brands-area section_gap_bottom">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                </div>
            </div>
        </div>
    </section>
    <!--================ End Brands Area =================-->


    <!--================Footer Area =================-->

    <!--================End Footer Area =================-->



    </body>
    @endsection




