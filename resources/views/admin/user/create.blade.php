@extends('admin.layouts.master')
@section('content')
    <form action="{{route('users.store')}}" METHOD="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-6 offset-md-3">
                @include('partials.form-errors')
                <div class="form-group">
                    <div class="small-12 medium-2 large-2 columns">
                        <div class="circle">
                            <!-- User Profile Image -->
                            <img class="profile-pic"
                                 src="http://cdn.cutestpaw.com/wp-content/uploads/2012/07/l-Wittle-puppy-yawning.jpg">

                            <!-- Default Image -->
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="p-image">
                            <i class="fa fa-camera upload-button"></i>
                            <input class="file-upload" name="avatar" type="file" accept="image/*" ref="file" "/>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="validationCustom01">نام کاربری:</label>
                    <input type="text" class="form-control" name="user_name" placeholder="برای مثال : alirostami"
                    >
                </div>
                <div class="form-group">
                    <label for="validationCustom01">نام و نام خانوادگی :</label>
                    <input type="text" class="form-control" name="name" placeholder="برای مثال : علی رستمی"
                    >
                </div>
                <div class="form-group">
                    <label for="validationCustom01">ایمیل :</label>
                    <input type="email" class="form-control" name="email" placeholder="برای مثال : alirostami@gmail.com"
                    >
                </div>
                <div class="form-group">
                    {!! Form::label('status', 'وضعیت کاربر را وارد کنید:') !!}
                    {!! Form::select('status', [1=>'فعال', 0=>'غیرفعال'], 0, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('roles', 'نقش را وارد کنید :') !!}
                    {!! Form::select('roles[]', $roles, null,['multiple'=>'multiple', 'class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>رمز عبور :</label>
                    <input type="password" class="form-control" name="password">
                    <small id="emailHelp" class="form-text text-muted">رمز عبور باید بیشتر از 6 کاراکتر باشد</small>
                </div>
                <div class="form-group">
                    <label>تایید رمز عبور :</label>
                    <input type="password" class="form-control" name="confirmed">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">ثبت کاربر</button>
                </div>
            </div>
        </div>
    </form>

@endsection




