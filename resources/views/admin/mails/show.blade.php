@extends('admin.layouts.master')


@section('content')
    <h3 class="p-b-2 text-center">پیام {{$mail->name}}</h3>

    <div class="row">
        <div class="col-md-9">
            @include('partials.form-errors')
        <form>
            <div class="form-group">
                {!! Form::label('title', 'موضوع:') !!}
                {!! Form::text('title', $mail->subject, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('title', 'نام:') !!}
                {!! Form::text('title', $mail->name, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('title', 'ایمیل:') !!}
                {!! Form::text('title', $mail->email, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'متن :') !!}
                {!! Form::textarea('description', $mail->message, ['class'=>'form-control']) !!}
            </div>

        </form>
        </div>


    </div>

@endsection
