@extends('admin.layouts.master')


@section('content')

    @if(Session::has('delete_post'))
        <div class="alert alert-danger">
            <div>{{session('delete_post')}}</div>
        </div>
    @endif
    @if(Session::has('add_post'))
        <div class="alert alert-success">
            <div>{{session('add_post')}}</div>
        </div>
    @endif
    @if(Session::has('update_post'))
        <div class="alert alert-success">
            <div>{{session('update_post')}}</div>
        </div>
    @endif
    <h3 class="p-b-2">لیست تماس ها</h3>
    <table class="table table-hover bg-content">
        <thead>
        <tr>
            <th>موضوع</th>
            <th>نام</th>
            <th>توضیحات</th>
            <th>ایمیل</th>
            <th>تاریخ ایجاد</th>
        </tr>
        </thead>
        <tbody>
        @foreach($mails as $mail)
            <tr>
                <td>{{$mail->subject}}</td>
                <td>{{$mail->name}}</td>
                <td><a href="{{route('contact.edit', $mail->id)}}">{{Illuminate\Support\Str::limit($mail->message, 50,' (...)')}}</a></td>
                <td>{{$mail->email}}</td>
                <td>{{\Hekmatinasser\Verta\Verta::instance($mail->created_at)->format('%B %d، %Y')}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="col-md-12" style="text-align: center">{{$mails->links()}}</div>
@endsection
