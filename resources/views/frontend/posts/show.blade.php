@extends('layouts.master')
@section('head')
    <meta name="description" content="{{$post->meta_description}}">
    <meta name="keywords" content="{{$post->meta_keywords}}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
@endsection
@section('blog')
<body>
<!--================ Start Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-center">
                <h2>{{$post->title}}</h2>
                <div class="page_link">
                    <a href="{{route('frontend.posts')}}">صفحه اصلی</a>
                    <a href="{{route('frontend.posts')}}">وبلاگ</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Banner Area =================-->

<!--================Blog Area =================-->
<section class="blog_area single-post-area section-margin">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-list">
                <div class="single-post">
                    <div class="feature-img">
                        <img class="img-fluid" src="{{$post->photo->path}}" alt="">
                    </div>
                    <div class="blog_details">
                        <h2>{{$post->title}}</h2>
                        <ul class="blog-info-link mt-3 mb-4">
                            <li><a href="#"><i class="ti-user"></i> {{$post->category->title}}</a></li>
                        </ul>
                        <p class="excert">
{{$post->description}}
                        </p>




                    </div>
                </div>
                <div class="navigation-top">
                    <div class="d-sm-flex justify-content-between text-center">
                        <ul class="social-icons">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                            <li><a href="#"><i class="ti-dribbble"></i></a></li>
                            <li><a href="#"><i class="ti-wordpress"></i></a></li>
                        </ul>
                    </div>
                </div>


                <div class="blog-author">
                    <div class="media align-items-center">
                        <img src="img/blog/author.png" alt="">
                    </div>
                </div>

                <!-- Single Comment -->
                @include('partials.comments', ['comments'=> $post->comments, 'post'=> $post])

                <div class="comment-form">
                    <h4>ارسال نظر</h4>

                        {!! Form::open(['method' => 'POST','class'=>'form-contact comment_form', 'route'=> ['frontend.comments.store', $post->id]]) !!}

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control w-100" name="description" id="comment" cols="30" rows="9" placeholder="نظر خود را بنویسید"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="name" id="name" type="text" placeholder="نام">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="email" id="email" type="email" placeholder="ایمیل">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="primary_btn button-contactForm">ارسال نظر</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">

                    <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">دسته بندی</h4>
                        <ul class="list cat-list">
                            <li>
                                @foreach($categories as $category)
                                <a href="#" class="d-flex">
                                    <p>{{$category->title}}</p>

                                </a>
                                    @endforeach
                            </li>
                        </ul>
                    </aside>

                    <aside class="single_sidebar_widget popular_post_widget">
                        <h3 class="widget_title">پست های اخیر</h3>
                        @foreach($recently as $v)
                            <div class="media post_item">

                                <img src="{{$v->photo->path}}" alt="post" style="height:80px">
                                <div class="media-body">
                                    <a href="{{route('frontend.posts.show', $post->slug)}}">
                                        <h3>{{Illuminate\Support\Str::limit($v->title,30)}}</h3>
                                    </a>
                                    <p>{{\Hekmatinasser\Verta\Verta::instance($v->created_at)->format('%B %d، %Y')}}</p>
                                </div>

                            </div>
                        @endforeach
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->


@endsection
