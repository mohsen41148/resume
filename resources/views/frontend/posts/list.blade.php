@extends('layouts.master')
@section('head')
    <meta name="description" content="وبلاگ هاناول">
    <meta name="keywords" content="مجله تخصصی لاراول ، مجله تخصصی وب سایت">
@endsection
@section('blog')

<body>

<!--================ Start Header Area =================-->


<!--================ Start Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-center">
                <h2>وبلاگ</h2>
                <div class="page_link">
                    <a href="index.html">صفحه اصلی</a>
                    <a href="blog.html">وبلاگ</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Banner Area =================-->

<!--================Blog Area =================-->
<section class="blog_area section-margin">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="blog_left_sidebar">
                    @foreach ($posts as $post)


                    <article class="blog_item">
                        <div class="blog_item_img">
                            <img class="card-img rounded-0" src="{{$post->photo->path}}" alt="">
                            <a href="#" class="blog_item_date">

                                <h3>{{\Hekmatinasser\Verta\Verta::instance($post->created_at)->format('%B %d، %Y')}}</h3>
                                <p></p>
                            </a>
                        </div>

                        <div class="blog_details">
                            <a class="d-inline-block" href="{{route('frontend.posts.show', $post->slug)}}">
                                <h2>{{$post->title}}</h2>
                            </a>
                            <p>{{Illuminate\Support\Str::limit($post->description,85)}}</p>
                            <ul class="blog-info-link">
                                <li><a href="#"><i class="far fa-user"></i> {{$post->category->title}}</a></li>
                            </ul>
                        </div>
                    </article>

                        @endforeach





                    <nav class="blog-pagination justify-content-center d-flex">
                        <ul class="pagination">
                            {{$posts->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
<!--
                    <aside class="single_sidebar_widget search_widget">
                        <form action="{{url('/search')}}">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" name="search" class="form-control" placeholder="">
                                    <div class="input-group-append">
                                        <button class="btn" type="button"><i class="ti-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <button class="primary_btn rounded-0 primary-bg text-white w-100" type="submit">جستجو</button>
                        </form>
                    </aside>
                    -->
                    <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">Category</h4>
                        <ul class="list cat-list">
                            <li>
                                @foreach($categories as $category)
                                <a href="#" class="d-flex">
                                    <p>{{$category->title}}</p>
                                    <p></p>
                                </a>
                                    @endforeach
                            </li>

                        </ul>
                    </aside>

                    <aside class="single_sidebar_widget popular_post_widget">
                        <h3 class="widget_title">پست های اخیر</h3>
                        @foreach($recently as $v)
                        <div class="media post_item">

                            <img src="{{$v->photo->path}}" alt="post" style="height:80px">
                            <div class="media-body">
                                <a href="{{route('frontend.posts.show', $post->slug)}}">
                                    <h3>{{Illuminate\Support\Str::limit($v->title,30)}}</h3>
                                </a>
                                <p>{{\Hekmatinasser\Verta\Verta::instance($v->created_at)->format('%B %d، %Y')}}</p>
                            </div>

                        </div>
                        @endforeach
                    </aside>






                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->

@endsection


