@extends('layouts.master')
@section('content')
    <!--================ Start Home Banner Area =================-->
    <section class="home_banner_area">
        <div class="banner_inner">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6">
                        <div class="banner_content">
                            <!-- <h3>Hey There !</h3> -->
                            <h3>{{__('msg.hi')}}  </h3>
                            <!-- <h1 class="text-uppercase">I am jo Breed</h1> -->
                            <h1 class="text-uppercase"> {{__('msg.name')}}</h1>
                            <h5 class="text-uppercase">{{__('msg.developer')}}</h5>
                            <div class="social_icons my-5">
                                <!--
                                    <a href="#"><i class="ti-twitter"></i></a>
                                    <a href="#"><i class="ti-gmail"></i></a>
                                    <a href="#"><i class="ti-instagram"></i></a>
                                    <a href="#"><i class="ti-dribbble"></i></a>
                                    <a href="#"><i class="ti-vimeo"></i></a>-->
                            </div>
                            <a class="primary_btn" href="{{route('about_me')}}"><span>{{__('msg.expertise')}}</span></a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="home_right_img">
                            <img class="img-fluid" src="{{asset('img/about-us.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Home Banner Area =================-->

    <!--================ Start Statistics Area =================-->
    <section class="statistics_area">
        <div class="container">
            <div class="row justify-content-lg-start justify-content-center">
                <div class="col-lg-2 col-md-3">
                    <div class="statistics_item">
                        <h3><span class="counter">15</span>k+</h3>
                        <p>{{__('msg.happy1')}}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="statistics_item">
                        <h3><span class="counter">12</span>k+</h3>
                        <p>{{__('msg.happy2')}}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3">
                    <div class="statistics_item">
                        <h3><span class="counter">9</span>/10</h3>
                        <p>{{__('msg.happy3')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Statistics Area =================-->

    <!--================ Start About Us Area =================-->
    <section class="about_area section_gap">
        <div class="container">
            <div class="row justify-content-start align-items-center">
                <div class="col-lg-5">
                    <div class="about_img">
                        <img class="" src="{{asset('img/banner/home-right.png')}}" alt="" >
                    </div>
                </div>

                <div class="offset-lg-1 col-lg-5">
                    <div class="main_title text-left">
                        <p class="top_text">{{__('msg.about_me')}}<span></span></p>
                        <h2>
                            full stack developer <br>

                        </h2>
                        <p>
                            {{__('msg.about_expertise')}}
                        </p>
                        <a class="primary_btn" href="{{route('about_me')}}"><span>{{__('msg.about_me')}}</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End About Us Area =================-->

    <!--================ Start Features Area =================-->
    <section class="services_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main_title">
                        <p class="top_text">{{__('msg.service')}}  <span></span></p>
                        <h2> {{__('msg.services')}}  <br>
                            {{__('msg.services_can')}} </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="service_item">
                        <img src="img/services/s1.png" alt="">
                        <h4>Web Development</h4>
                        <p>{{__('msg.service1')}}
                           </p>
                        <a href="{{route('about_me')}}" class="primary_btn2 mt-35">"{{__('msg.read_more')}}</a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="service_item">
                        <img src="img/services/s2.png" alt="">
                        <h4>consultation</h4>
                        <p>{{__('msg.service2')}}
                           </p>
                        <a href="{{route('about_me')}}" class="primary_btn2 mt-35">"{{__('msg.read_more')}}</a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                    <div class="service_item">
                        <img src="img/services/s3.png" alt="">
                        <h4>API & SOCKET</h4>
                        <p>
                            {{__('msg.service3')}}
                        </p>
                        <a href="{{route('about_me')}}" class="primary_btn2 mt-35">"{{__('msg.read_more')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Features Area =================-->


    <!--================ Start Testimonial Area =================-->
    <section class="testimonial_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main_title">
                        <p class="top_text">{{__('msg.attributes')}} <span></span></p>
                        <h2>{{__('msg.features')}} </h2>
                    </div>
                </div>
            </div>

            <div class="owl-carousel owl-theme testimonial-slider ">
                <div class="testimonial-item">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="testi-img mb-4 mb-lg-0">
                                <img src="img/banner/1200px-Laravel.svg.png" alt="">
                            </div>
                        </div>
                        <div class="offset-lg-1 col-lg-5">
                            <div class="main_title text-left">
                                <p class="top_text">Laravel<span></span></p>
                                <h2>
                                    {{__('msg.backend')}}
                                </h2>
                                <p>
                                    {{__('msg.laravel_future')}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="testi-img mb-4 mb-lg-0">
                                <img src="img/banner/1200px-Vue.js_Logo_2.svg.png" alt="">
                            </div>
                        </div>
                        <div class="offset-lg-1 col-lg-5">
                            <div class="main_title text-left">
                                <p class="top_text">Laravel<span></span></p>
                                <h2>
                                    {{__('msg.vue')}}
                                </h2>
                                <p>
                                    {{__('msg.vue_future')}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Testimonial Area =================-->

    <!--================ Start Blog Area =================-->
    <section class="blog-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main_title">
                        <p class="top_text">Our blog <span></span></p>
                        <h2>Latest Story From <br>
                            Our Blog </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach ($recently as $post)


                <div class="col-lg-4 col-md-6">
                    <div class="single-blog">
                        <div class="thumb">
                            <img class="img-fluid" src="{{$post->photo->path}}" alt="">
                        </div>
                        <div class="short_details">
                            <div class="meta-top d-flex">
                                <a href="#"><i class="ti-user"></i> $posts->user->name</a>
                                <a href="#"><i class="ti-calendar"></i>{{\Hekmatinasser\Verta\Verta::instance($post->created_at)->format('%B %d')}}</a>
                            </div>
                            <a class="d-block" href="{{route('frontend.posts.show', $post->slug)}}">
                                <h4>{{Illuminate\Support\Str::limit($post->title,45)}}</h4>
                            </a>
                            <div class="text-wrap">
                                <p>
                                    {{Illuminate\Support\Str::limit($post->description,85)}}
                                </p>
                            </div>
                            <a href="{{route('frontend.posts.show', $post->slug)}}" class="primary_btn2">"{{__('msg.read_more')}}</a>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>
    <!--================ End Blog Area =================-->
    @endsection
