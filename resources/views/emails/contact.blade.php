<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" >
</head>
<body>
<div class="card border-success mb-3" style="max-width: 18rem;">
    <div class="card-header bg-transparent border-success">From {{$data['email']}}</div>
    <div class="card-body text-success">
        <h5 class="card-title">{{$data['subject']}}</h5>
        <p class="card-text">{{$data['message']}}</p>
    </div>
    <div class="card-footer bg-transparent border-success"> {{$data['name']}}</div>
</div>
</body>
</html>
