<?php
return
    [
        'home'=>'صفحه اصلی',
        'about'=>'بیشتر',
        'blog'=>'بلاگ',
        'language'=>'language',
        'persian'=>'فارسی',
        'english'=>'English',
        'hi'=>'سلام',
        'name'=>'محسن بابائی هستم',
        'developer'=>'برنامه نویس وب',
        'expertise'=>'تخصص من',
        'happy1'=>'مشتریان خوشحال',
        'happy2'=>'مسئله حل شده',
        'happy3'=>'میانگین امتیاز',
        'full_stack'=>'توسعه دهنده صفر تا صد وب سایت',
        'about_me'=>'درباره من',
        'about_expertise'=>' برنامه نویس وب متخصص لاراول انجام پروژه های تحت وب ، مشاوره در زمینه وب سایت',
        'service'=>'خدمات',
        'services'=>'خدماتی',
        'services_can'=>' که میتوانم ارائه دهم',
        'service1'=>'طراحی وب سایت با امنیت و کارایی بالا با معماری mvc',
        'service2'=>'مشاوره برای ایجاد و بهبود وب سایت',
        'service3'=>'و برنامه نویسی سوکت
                            API',
        'attributes'=>'ویژگی ها',
        'features'=>'ویژگی های زبان های مورد استفاده',
        'laravel'=>'لاراول',
        'backend'=>'محبوب ترین فریمورک بکند ',
        'laravel_future'=>'                                   طی سالهای اخیر و همچنین در حال حاضر پر طرفدارین فریمورک جهان شناخته شده بطوریکه سایت های محبوب از جمله دیجی کالا با این فریمورک نوشته شده است
',
        'vue'=>'محبوب ترین فریمورک طراحی رابط کاربری',
        'vue_future'=>'یکی از فریمورک های جاوااسکریپت که برای انعطاف پذیری بالا و ساده بودن آن در میان برنامه نویسان محبوب است از همه مهمتر با لاراول سازگار تر است',
        'read_more'=>'ادامه مطلب',
        //contact
        'click'=>'کیلک کن',
        'contact'=>'تماس با من',
        'message'=>'پیام خود را وارد کنید',
        'your_name'=>'نام خود را وارد کنید',
        'email'=>'ایمیل خود را وارد کنید',
        'subject'=>'موضوع ',
        'send'=>' ارسال پیام ',
        'add1'=>' ایران ، البرز ',
        'add2'=>' کرج آزادگان ',
        'time_to_answer_phone'=>'ساعت پاسخگویی از 6 تا 22  ',
        'time_to_answer_email'=>'هر زمانی که دوست داشتین پیام بدین',
        //about me
        'content1'=>'دانشجوی نرم افزار دانشگاه  کرج با پنج هزار ساعت  سابقه برنامه نویسی در زمینه وب بصورت حرفه ای ',
        'content2'=>' عاشق برنامه نویسی هستم مخصوصا در زمینه وب و چالش های اون رو دوست دارم و  همیشه در حال یادگیری هستم',
        'content3'=>' تخصص من در فریمورک لاراول و همجنین طراحی پایگاه داده هست ، خوشحال میشم برای ایجاد یا بهبود وب سایتتون مشاوره بدم ',
        'content4'=>'من اینجام تا ایده های شما رو به واقعیت تبدیل کنم  ',
        'my_details'=>' مشخصات من',
        'side1'=>' نام :',
        'side2'=>' تحصیلات :',
        'side3'=>' تخصص :',
        'education'=>' دانشجوی کارشناسی نرم افزار',

    ]
?>

