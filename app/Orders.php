<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public $timestamps=false;
    public function distributor()
    {
        return $this->belongsToMany(Distributors::class);
    }

    public function parts()
    {
        return $this->belongsToMany(Parts::class);
    }
}
