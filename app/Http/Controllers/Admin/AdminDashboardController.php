<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Comment;
use App\Distributors;
use App\Http\Controllers\Controller;
use App\Orders;
use App\Photo;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $userCount=User::query()->count();
        $categoryCount=Category::query()->count();
        $postCount=Post::query()->count();
        $commentCount=Comment::query()->count();
        $user=Auth::user();
        $photo=Photo::query()->pluck('id','path') ;

        return view('admin.dashboard.index',compact(['user','userCount','postCount','categoryCount','commentCount']));
    }


}
