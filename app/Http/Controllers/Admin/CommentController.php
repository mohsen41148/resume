<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{
    public function index(){
        $comments = Comment::with('post')
            ->orderBy('created_at', 'desc')
            ->paginate(30);

        return view('admin.comments.index', compact(['comments']));
    }

    public function actions(Request $request, $id)
    {
        if($request->has('action')){
            if($request->input('action') == 'approved'){
                $comment = Comment::query()->findOrFail($id);
                $comment->status = 1;
                $comment->save();
                Alert::success('نظر کاربر تایید شد', 'Success Message');
            }else{
                $comment = Comment::query()->findOrFail($id);
                $comment->status = 0;
                $comment->save();
                Alert::success('نظر کاربر تایید نشد', 'Success Message');
            }
        }
        return redirect('/admin/comments');
    }

    public function edit($id)
    {
        $comment = Comment::query()->findOrFail($id);
        return view('admin.comments.edit', compact(['comment']));
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::query()->findOrFail($id);
        $comment->description = $request->input('description');
        $comment->save();

        Session::flash('update_comment', 'نظر با موفقیت ویرایش شد');
        return redirect('/admin/comments');
    }

    public function destroy($id)
    {
        $comment = Comment::query()->findOrFail($id);
        $comment->delete();
        Session::flash('delete_comment', 'نظر با موفقیت حذف شد');

        return redirect('admin/comments');
    }
}
