<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserRequest;
use App\Photo;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Hekmatinasser\Verta\Verta;



class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users=User::query()->paginate(15);
        return view('admin.user.list',compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles=Role::query()->pluck('name','id');
        return view('admin.user.create',compact(['roles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {


            $user = new User();

            if($file = $request->file('avatar')){
                $name = time() . $file->getClientOriginalName() ;
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->user_id = Auth::id();
                $photo->save();

                $user->photo_id = $photo->id;
            }

            $user->name = $request->input('name');
            $user->user_name = $request->input('user_name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->status = $request->input('status');
            $user->save();
            $user->roles()->attach($request->input('roles'));
            Alert::success('با موفقیت ذخیره شد !', 'Success Message');
            return redirect()->route('users.create');






    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user=User::query()->findOrFail($id);
        $roles=Role::query()->pluck('name','id');
        return view('admin.user.edit',compact(['user','roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserEditRequest $request, $id)
    {
        $user = User::query()->findOrFail($id);

            if($file = $request->file('avatar')){
                $name = time() . $file->getClientOriginalName() ;
                $file->move('images', $name);
                $photo = new Photo();
                $photo->name = $file->getClientOriginalName();
                $photo->path = $name;
                $photo->user_id = Auth::id();
                $photo->save();

                $user->photo_id = $photo->id;
            }

            $user->name = $request->input('name');
            $user->user_name = $request->input('user_name');
            $user->email = $request->input('email');

            if(trim($request->input('password') != "")){
                $user->password = bcrypt($request->input('password'));
            }

            $user->status = $request->input('status');
            $user->save();
            //$user->roles()->sync($request->input('roles'));
            $user->roles()->sync($request->input('roles'));
            Alert::success('با موفقیت ویرایش شد !', 'Success Message');
            return redirect()->route('users.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {

        $user=User::query()->findOrFail($id);
        $user->delete();
        Alert::success('با موفقیت حذف شد !', 'Success Message');
        return redirect()->route('users.index');
    }
}
