<?php

namespace App\Http\Controllers\Frontend;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CommentController extends Controller
{
    public function store(Request $request, $postId)
    {
        $post = Post::query()->findOrFail($postId);
        if($post){
            $comment = new Comment();
            $comment->description = $request->input('description');
            $comment->email = $request->input('email');
            $comment->name = $request->input('name');
            $comment->post_id = $post->id;
            $comment->status = 0;
            $comment->save();
        }
        Alert::success('نظر شما با موفقیت درج شد و در انتظار تایید مدیران قرار گرفت', 'Success Message');
        return back();
    }
    public function reply(Request $request)
    {
        $postId = $request->input('post_id');
        $parentId = $request->input('parent_id');

        $post = Post::query()->findOrFail($postId);
        if($post){
            $comment = new Comment();
            $comment->description = $request->input('description');
            $comment->parent_id = $parentId;
            $comment->post_id = $post->id;
            $comment->status = 0;
            $comment->save();
        }

        Alert::success('نظر شما با موفقیت درج شد و در انتظار تایید مدیران قرار گرفت', 'Success Message');
        return back();
    }
}
