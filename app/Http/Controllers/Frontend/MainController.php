<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    public function posts(Request $request)
    {
       $posts = Post::with('user', 'category', 'photo')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        $recently = Post::with('user', 'category', 'photo')
            ->where('status', 1)
            ->orderBy('created_at', 'desc')->paginate(5);
        $categories=Category::all();

        return view('frontend.posts.list' , compact(['posts', 'recently','categories']));
    }

    public function about_me()
    {
        return view('partials.about_me');
    }

}
