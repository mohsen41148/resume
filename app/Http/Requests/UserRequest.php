<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'user_name' => 'required|unique:posts',
            'email' => 'required|email',
            'roles' => 'required',
            'status' => 'required',
            'password' => 'required|min:6',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'لطفا نام و نام خانوادگی را وارد کنید',
            'user_name.required' => 'لطفا نام کاربری را وارد کنید',
            'user_name.unique'=>'این نام کاربری قبلا ثبت شده است',
            'email.required' => 'لطفا ایمیل را وارد کنید',
            'email.email' => 'ایمیل شما معتبر نیست',
            'roles.required' => 'لطفا حداقل یک نقش را انتخاب  کنید',
            'status.required' => 'لطفا وضعیت کاربر را انتخاب کنید',
            'password.required' => 'لطفا رمز عبور را وارد کنید',
            'password.min' => 'رمز عبور شما باید بیش از ۶ کاراکتر باشد',
            'avatar.image'=>'فرمت عکس قابل قبول است .'
        ];
    }

}
