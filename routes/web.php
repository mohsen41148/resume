<?php

use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

//blog route
Route::get('/posts', 'Frontend\MainController@posts')->name('frontend.posts');
Route::get('/posts/{slug}', 'Frontend\PostController@show')->name('frontend.posts.show');
Route::post('comments/{postId}', 'Frontend\CommentController@store')->name('frontend.comments.store');
Route::post('comments}', 'Frontend\CommentController@reply')->name('frontend.comments.reply');
Route::get('search', 'Frontend\PostController@searchTitle' )->name('frontend.posts.search');



// personal-site route

Route::get('/', function () {
    $recently = Post::with('user', 'category', 'photo')
        ->where('status', 1)
        ->orderBy('created_at', 'desc')->paginate(3);
    return view('welcome', compact('recently'));
})->name('home');
Route::get('/about_me', 'Frontend\MainController@about_me')->name('about_me');
Route::get('/logout', 'Auth\LoginController@logout');
Route::resource('contact','Frontend\MailController');


// admin route
Route::group(['before' => 'auth', 'middleware' => 'admin', 'prefix' => 'admin'], function () {
    Route::get('dashboard', 'Admin\AdminDashboardController@index')->name('dashboard');
    Route::resource('users', 'Admin\AdminUserController');
    Route::resource('posts', 'Admin\AdminPostController');
    Route::resource('categories', 'Admin\AdminCategoryController');
    Route::resource('photos', 'Admin\AdminPhotoController');
    Route::get('photos/upload', ['as' => 'photos.upload', 'use' => 'Admin\AdminPhotoController@upload']);
    Route::get('comments', 'Admin\CommentController@index')->name('comments.index');
    Route::post('actions/{id}', 'Admin\CommentController@actions')->name('comments.actions');
    Route::get('comments/{id}', 'Admin\CommentController@edit')->name('comments.edit');
    Route::patch('comments/{id}', 'Admin\CommentController@update')->name('comments.update');
    Route::delete('comments/{id}', 'Admin\CommentController@destroy')->name('comments.destroy');
    Route::delete('delete/media', 'Admin\AdminPhotoController@deleteAll')->name('photo.delete.all');
});
Route::get('locale/{locale}', function ($locale) {
  \Illuminate\Support\Facades\Session::put('locale',$locale);
  return redirect()->back();
});
